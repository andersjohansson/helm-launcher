;;; helm-launcher.el --- Launch apps etc. with helm  -*- lexical-binding: t; -*-

;; Copyright (C) 2018  Anders Johansson

;; Author: Anders Johansson <anders.johansson@physics.uu.se>
;; Keywords: convenience, unix

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
(require 'cl-macs)
(require 'subr-x)
(require 'helm)
(require 'helm-for-files)

(defgroup helm-launcher nil ""
  :group 'helm)

(defcustom helm-launcher-desktop-directories '("/usr/share/applications" "~/.local/share/applications")
  "List of directories where .desktop files can be found"
  :type '(repeat directory))

(defcustom helm-launcher-mime-open-executable "xdg-open"
  "Executable for opening files via mime-type.
 Something like xdg-open or gnome-open"
  :type 'string)

(defcustom helm-launcher-icon-size 48
  "Icon size used for applications in ‘helm-launcher’."
  :type 'integer)

(defcustom helm-launcher-sources
  '(helm-launcher-desktop-applications-source
    helm-launcher-file-history-source
    helm-launcher-recentf-mime-source
    helm-launcher-currentdir-source)
  "List of sources to use for ‘helm-launcher’"
  :type '(repeat symbol))

;;; Command
;;;###autoload
(defun helm-launcher ()
  (interactive)
  (helm :sources helm-launcher-sources
        :prompt "Launch: "))


;;; Source: application launcher

(defvar helm-launcher--get-icons-command
  (expand-file-name "get-icons.py"
                    (or (and load-file-name
                             (file-name-directory load-file-name))
                        default-directory))
  "Command name for invoking the python script “get-icons.py”.
Should be automatically initialized to point to the script in the
directory of “helm-launcher.el”")

(defun helm-launcher--icon-image (filename)
  (propertize " "
              'display
              `(image :type ,(image-type filename) :margin 0 :ascent center :file ,filename
                      :height ,helm-launcher-icon-size)))

(defun helm-launcher--get-icons (iconlist)
  (let ((buf (generate-new-buffer " *helm-launchers-icons*")))
    (if (eq 0 (apply #'call-process helm-launcher--get-icons-command nil (list buf nil) nil
                     (number-to-string helm-launcher-icon-size)
                     iconlist))
        (with-current-buffer buf
          (goto-char (point-min))
          (prog1
              (read buf)
            (kill-buffer buf))))))

(defun helm-launcher--iconify (applist)
  (let ((icons (helm-launcher--get-icons (mapcar #'cadr applist))))
    (cl-loop for app in applist
             collect (cons (concat (helm-launcher--icon-image (pop icons)) " " (car app) ) (nth 2 app)))))

(defun helm-launcher--get-desktop-applications ()
  (let ((namelang (format "^Name\\[%s\\]=\\(.+\\)$" (substring (getenv "LANG") 0 2))))
    (cl-loop for file in (cl-loop for dir in helm-launcher-desktop-directories
                                  append (directory-files dir t "\\.desktop$" t))
             collect
             (let ((basename (file-name-base file))
                   icon localname name
                   )
               (with-temp-buffer
                 (insert-file-contents file)
                 (let ((limit (if (search-forward "[Desktop Action" nil t)
                                  (point)
                                (point-max))))
                   (goto-char (point-min))
                   (setq icon (if (search-forward "Icon=" limit t)
                                  (buffer-substring (point) (point-at-eol))
                                "FAILED"))
                   (goto-char (point-min))
                   (when (search-forward-regexp namelang limit t)
                     (setq localname (match-string 1)))
                   (goto-char (point-min))
                   (when (search-forward-regexp "^Name=\\(.+\\)$" limit t)
                     (setq name (match-string 1)))

                   ;; The following will allow us to get matches also
                   ;; for the name in english if we’re displaying a
                   ;; localized name
                   (when localname
                     (setq name (concat localname (propertize (concat " " name) 'invisible t))))
                   ;; And matching the basename of the desktop file (for example "Evince")
                   (setq name
                         (concat
                          name
                          (propertize
                           (concat " "
                                   ;; This gets "Evince" out of "org.gnome.Evince"
                                   (car (last (split-string basename "\\."))))

                           'invisible t)))
                   ;; Some names in my files had soft hyphens (­). Remove:
                   (setq name (replace-regexp-in-string "­" "" name t t))))
               (list name icon basename))
             into apps
             finally return
             (thread-first
                 apps
               (cl-sort #'string-collate-lessp :key #'car)
               (helm-launcher--iconify)))))

(defvar helm-launcher--desktop-applications nil
  "Stored value of desktop-applications")

(defun helm-launcher-update-applist ()
  (interactive)
  (setq helm-launcher--desktop-applications
        (helm-launcher--get-desktop-applications)))

(defun helm-launcher--desktop-applications ()
  (or helm-launcher--desktop-applications
      (helm-launcher-update-applist)))

(defvar helm-launcher-desktop-applications-history nil)

(defvar helm-launcher-desktop-applications-source
  (helm-build-sync-source "Applications"
    :candidates #'helm-launcher--desktop-applications
    :action #'helm-launcher-launch-application
    :history 'helm-launcher-desktop-applications-history)
  "")

(defun helm-launcher-launch-application (_s)
  (cl-loop for c in (helm-marked-candidates)
           do (call-process-shell-command (format "gtk-launch %s" c) nil 0)))

;;; Source: Helm-launch history
(defvar helm-launcher-file-history nil
  "The history of launched files in ‘helm-launcher’")
(put 'helm-launcher-file-history 'history-length 300)
(with-eval-after-load 'savehist
  (push 'helm-launcher-file-history savehist-additional-variables))

(defvar helm-launcher-file-history-source
  (helm-build-sync-source "History"
    :candidates 'helm-launcher-file-history
    :action
    '(("Launch" . helm-launcher-launch-mime)
      ("Remove from list" . helm-launcher-remove-from-file-history))))

(defun helm-launcher-remove-from-file-history (_candidate)
  (cl-loop for cand in (helm-marked-candidates)
           do (setq helm-launcher-file-history (delete cand helm-launcher-file-history))))

;;; Source: Recentf

(defvar helm-launcher-recentf-list nil)

(defvar helm-launcher-recentf-mime-source
  (helm-make-source "Recentf mime" 'helm-recentf-source))

(helm-add-action-to-source "Launch" #'helm-launcher-launch-mime helm-launcher-recentf-mime-source 0)


;;; Source currentdir

(defvar helm-launcher-currentdir-source
  (helm-make-source "Currentdir mime" 'helm-files-in-current-dir-source))

(helm-add-action-to-source "Launch" #'helm-launcher-launch-mime helm-launcher-currentdir-source 0)

;;; Launch files via mime
(defun helm-launcher-launch-mime (_s)
  (cl-loop for cand in (helm-marked-candidates)
           do (call-process helm-launcher-mime-open-executable nil 0 nil cand)
           (add-to-history 'helm-launcher-file-history cand)
           ;; (call-process-shell-command (format "xdg-open %s" c) nil 0)
           ))


(provide 'helm-launcher)
;;; helm-launcher.el ends here
