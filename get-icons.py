#!/usr/bin/env python3
import sys
import gi

sys.argv.pop(0)
size = int(sys.argv.pop(0))
icons = sys.argv
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


icon_theme = Gtk.IconTheme.get_default()
print ('(')

for i in icons:
    icon = icon_theme.lookup_icon(i, size, 0)
    if icon is None:
        icon = icon_theme.lookup_icon('system-run', size, 0)
    if icon is None:
        print('""')
    else:
        print('"' + icon.get_filename() + '"')

print(')')
